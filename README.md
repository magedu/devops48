#### 介绍
马哥教育D48期专用

#### 使用说明

##### 不要删除别人的代码！ 目录结构
                     
```
每次作业请创建单独的目录，例如
第01周/D48001-张三/学习计划.txt
第02周/D48001-张三/example.py
```

##### 1.安装git
```
第一次使用：         
centos：       
  yum install git       
  
  
Ubuntu：       
  apt-get install git
  
git clone git@gitee.com:magedu/python33.git

更新本地代码 git pull

查看代码状态 git status

后面添加代码，只需要下面三行即可：
  git pull //拉取最新的代码文件
  git add .     
  git commit -m "first commit" //第一次提交  
  git pull //！！！ 提交之前，先同步下最新版的代码，避免冲突
  git push -u origin master //同步到远程服务器      

用命令行操作，要添加ssh的公钥到gitee里，操作方法

点击这个链接 http://git.mydoc.io/#text_154712
```

#### 码云特技

1.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
