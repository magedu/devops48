import socket

def send_request(operator, num1, num2):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        request = f"{operator}:{num1}:{num2}"
        s.sendall(request.encode())
        data = s.recv(1024)
    return data.decode()

if __name__ == "__main__":
    HOST = '127.0.0.1'
    PORT = 65432

    num1 = int(input("输入第一个数字: "))
    operator = input("输入运算符(+ or -): ")
    num2 = int(input("输入第二个数字: "))

    result = send_request(operator, num1, num2)
    print(f"服务端返回的结果是: {result}")
