import socket

def perform_operation(operator, num1, num2):
    if operator == '+':
        return num1 + num2
    elif operator == '-':
        return num1 - num2
    else:
        return None

def start_server(host, port):
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((host, port))
    server_socket.listen(1)

    print(f"Server listening on {host}:{port}")

    conn, addr = server_socket.accept()
    print(f"Connected by {addr}")

    with conn:
        while True:
            data = conn.recv(1024)
            if not data:
                break

            operator, num1, num2 = data.decode().split(':')
            num1, num2 = int(num1), int(num2)

            result = perform_operation(operator, num1, num2)
            conn.sendall(str(result).encode())

if __name__ == "__main__":
    HOST = '127.0.0.1'
    PORT = 65432

    start_server(HOST, PORT)
