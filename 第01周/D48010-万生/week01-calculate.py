def calculate():
    num1 = int(input("请输入第一个数字:"))
    fuhao = input("请输入+-*/:")
    if fuhao == '+' or '-' or '*' or '/':
        result = None
        num2 = int(input("请输入第二个数字:"))
        if fuhao == '+':
            result = num1 + num2
        elif fuhao == '-':
            result = num1 - num2
        elif fuhao == '*':
            result = num1 * num2
        elif fuhao == '/':
            result = num1 / num2
        print(result)
    else:
        print("输入的符号错误")
    jixu = input('yes or no:')
    if jixu == 'yes':
        calculate()
    else:
        print("感谢您的使用")
calculate()

# 2023-06-28 15:00:22 重新写一遍

def handle_input(times=3):
    num = input("请输入数字:")
    if not num.isdigit():
        if times == 1:
            print("输入的次数过多，自动退出")
            exit()
        print("输入值的类型错误，请输入数字")
        return handle_input(times - 1)
    return int(num)

def handle_operator(times=3):
    operator = input("请输入运算符号：")
    if operator not in ["+", "-", "*", "/"]:
        if times == 1:
            print("输错的次数过多，自动退出")
            exit()
        print("请输入正确是运算符号")
        return handle_operator(times - 1)
    return operator

while True:
    num1 = handle_input()
    operator = handle_operator()
    num2 = handle_input()
    if operator == "+":
        print("结果：", num1 + num2)
    if operator == "-":
        print("结果：", num1 - num2)
    if operator == "*":
        print("结果：", num1 * num2)
    if operator == "/":
        print("结果：", num1 / num2)
    y = input("是否继续，输入y/n:")
    if y.upper() != 'Y':
        break