while True:
  one_number = int(input("please enter one number:"))
  operation_symbol = input("please enter operation symbol:(+ - * /)")
  two_number = int(input("please enter one number again:"))
  if operation_symbol == "/":
      result = one_number / two_number
  elif operation_symbol == "+":
    result = one_number + two_number
  elif operation_symbol == "-":
    result = one_number - two_number
  elif operation_symbol == "*":
    result = one_number * two_number
  
  print(f"{one_number} {operation_symbol} {two_number} = {result}")
  
  if input("enter 'exit' to exit:") == 'exit':
    break
