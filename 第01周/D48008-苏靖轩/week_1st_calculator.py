#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# 实现一个简易计算器
# Copyright 2023 D48008-苏靖轩 <sujx@live.cn>
"""
第一周作业：
1. 通过本周的学习和自己查阅，你知道linux有哪些发行版本？
2. 实现一个简易的计算器，效果如下：
运行后提示让用户输入一个数字
   1. 提示输入操作符（+ - * /）
   2. 再次提示输入一个数字
   3. 打印计算结果
   4. 在不退出程序的前提下，可以允许用户继续输入新一组数据计算
"""

from sys import exit 
from textwrap import dedent

def start():
    print(dedent("""
          欢迎使用本计算器。
          本计算器每次只能接受一次运算。
          请输入第一个数字之后再输入运算符，再输入第二个数目字。
          请输入整数，不要使用文字或者小数。
          如果需要离开，请同时按下Ctrl和C键。
          """))
    
def compute():
    num_a = 0
    num_b = 0
    resualt = 0

    print("请输入第一个数字：")
    num_a = int(input(">> "))
    print("请输入运算符：")
    action = input("|| ")
    print("请输入第二个数字：")
    num_b = int(input(">> "))

    if action == "+":
        resualt = num_a + num_b
        print(f"运算结果为：\n {resualt}")
    elif action == "-":
        resualt = num_a - num_b
        print(f"运算结果为：\n {resualt}")
    elif action == "/":
        resualt = num_a / num_b
        print(f"运算结果为：\n {resualt}")
    elif action == "*":
        resualt = num_a * num_b
        print(f"运算结果为：{resualt}")
    else:
        print("请检查运算符是否正确输入！")
        compute()


def restart():
    print("是否进行下一轮计算？请输入Yes 或者 No")
    choice = input(">> ")

    if choice == "Yes":
        compute()
    else:
        print("再见！\n")
        exit(0)

start()
compute()
restart()
