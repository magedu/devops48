"""
Linux发行版本

红帽   Centos   Ubuntu  OpenEuler   debian

"""


# 计算器

def into(msg, t):
    while 1:
        In = input(msg).strip()
        if In == "":
            print("不能为空")
            continue
        if In == "q":
            exit()
        if t == "num" and In.isnumeric():
            a = float(In)
            return a
        elif t == "num":
            print("输入不合法，请重新输入！")
        else:
            return In


while True:
    x = into("请输入一个数字：", "num")
    while True:
        s = into("请输入一个算数运算符（+ - * /）：", "str")
        if s not in ["+", "-", "*", "/"]:
            print("运算符不合法")
            continue
        else:
            break
    while True:
        y = into("请输入一个数字：", "num")
        if s == "/" and y == 0:
            print("除法的除数不能为零，请重新输入")
            continue
        else:
            break

    if s == "+":
        print("结果为：", x + y)
    elif s == "-":
        print("结果为：", x - y)
    elif s == "*":
        print("结果为：", x * y)
    elif s == "/":
        print("结果为：", x / y)
    else:
        print("运算失败")
        continue
    print("请继续计算下一组(退出请输入q)")
