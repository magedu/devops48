# -*-coding:utf-8-*-
# @Time:2023/9/22 19:56
# @Author:"HeBin"
# @File:calculator.py
"""
第一周作业：
1. 通过本周的学习和自己查阅，你知道linux有哪些发行版本？
2. 实现一个简易的计算器，效果如下：
运行后提示让用户输入一个数字
   1. 提示输入操作符（+ - * /）
   2. 再次提示输入一个数字
   3. 打印计算结果
   4. 在不退出程序的前提下，可以允许用户继续输入新一组数据计算
"""
"""
linux发行版本：
1、CentOS 2、Ubuntu 3、RedHat 4、Debian 5、Fedora等
"""

# x = int(input('First number:' ))
# y = int(input('Second number:' ))
# s = input('You can choose "+", "-", "*", "/" to calculate:')
#
# if s == '+':
#     print('x + y =', x + y)
# elif s == '-':
#     print('x - y =', x -y)
# elif s == '*':
#     print('x * y =', x * y)
# elif s == '/':
#     print('x / y =', x /y)
print("""Welcome to use this sample calculator!!!\nYou can choose "+","-","*","/" to perform a sample calculate!""")
while True:
    try:
        x = int(input("First number:" ))
        s = input("Operator:" )
        y = int(input("Second number:" ))

        if s == '+':
            print("{}{}{}={}".format(x, s, y, x + y))
        elif s == '-':
            print("{}{}{}={}".format(x, s, y, x - y))
        elif s == '*':
            print("{}{}{}={}".format(x, s, y, x * y))
        elif s == '/':
            print("{}{}{}={}".format(x, s, y, x / y))
        else:
            print("Wrong input!")
    except ValueError:
        print("ValurError； Please input a integer")
        break
