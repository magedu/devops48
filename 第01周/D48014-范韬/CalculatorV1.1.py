#实现目的：计算 x与y的四则运输
#实现内容：
    #1）除数不能为0; 同时需要排除0.0.... 的类似输入
    #2）校验用户输入是否为浮点数，同时需要避免出现 “1.2.3” 等类似的错误输入
    #3）排除只有 “.”  和 “-”
    #4）仅支持四则运算
    #5) 支持输入负数
    #5）程序一旦运行不会自动停止直到用户手动关闭


while True:
    
    # 对x的输入进行校验
    while True:  
        alpha = 0
        digit = 0
        input_error_flag = 0  # 标记常量
        x = input("Plaease input the FIRST digital to opreate: ").strip('. /t/r/n/')    # 等待用户输入,常规去除尾巴
        
        for i in x:    # 判断用户输入内容是否在合理范围：只能在数字0-9和“.”，”-“ 内
            if i not in ['0','1','2','3','4','5','6','7','8','9','.','-'] or x.count(".") > 1:   
                input_error_flag += 1
                print("input error 1, try again.")
                break       # 一旦检测到非法输入即刻跳出For循环，不再对后续遍历
                
        else:
            for i in x:
                if i.isdigit():
                    digit += 1
                else: alpha += 1
        
        if len(x)==alpha:    # 判断是否只输入“.” 和“-”
            input_error_flag += 1
            print("input error 2, try again.")
            continue
        
        elif "-" in x and x.rfind('-') != 0 :    # 负号检测：find一定要从最右边开始，排除-1-的错误输入。思路：存在且位置为0
            input_error_flag += 1
            print("input error 3, try again.")
            continue
            
        elif not input_error_flag:  # 如果非法输入标记为0，意为无错误，则用户输入合法，跳出While循环
            break
    
    # 对运算符号校验
    while True:
        fun = input("which function do you want to run? +|-|*|/: ")
        if fun in ("+","-","*","/"):
            break
        else: print("input error 4, try again.")
    
    # 对y的输入进行校验
    while True:
        alpha = 0
        digit = 0
        input_error_flag = 0
        y = input("Plaease input the LAST digital to opreate: ").strip('. /t/r/n/')
        
        for i in y:
            if i not in ['0','1','2','3','4','5','6','7','8','9','.','-'] or y.count(".") > 1:    
                input_error_flag += 1
                print("input error 5, try again.")
                break      
                
        else:
            for i in y:
                if i.isdigit():
                    digit += 1
                else: alpha += 1
                    
        if len(y)==alpha:    # 判断是否只输入“.” 和“-”
            input_error_flag += 1
            print("input error 6, try again.")
            continue
                    
        elif "-" in y and y.rfind('-') != 0 :    # 负号检测
            input_error_flag += 1
            print("input error 7, try again.",y)
            continue
            
        elif input_error_flag:    # 短路后续代码，提高效率
            print("input error 8, try again.")
            continue
            
        elif fun =="/" and float(y) == 0.0:   # 0 不能做除数; 此时因为已经校验用户输入，故float(y)不会报错
            print("0 can not used as devisor. Please try again.")
            continue
       
        else:break
    
    # 计算并输出
    if fun == '+' :
        print("{}+{}={}".format(x,y,float(x)+float(y)))
    elif fun == "-" :
        print("{}-{}={}".format(x,y,float(x)-float(y)))
    elif fun == "*" :
        print("{}*{}={}".format(x,y,float(x)*float(y)))
    elif fun == "/" :
        print("{}/{}={}".format(x,y,float(x)/float(y)))