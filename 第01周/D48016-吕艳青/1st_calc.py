"""
实现一个简易的计算器，效果如下：
运行后提示让用户输入一个数字
   1. 提示输入操作符（+ - * /）
   2. 再次提示输入一个数字
   3. 打印计算结果
   4. 在不退出程序的前提下，可以允许用户继续输入新一组数据计算
"""

while True:
    try:
        num1 = int(input("请输入一个整数："))
        calc = input("请输入操作符(+, -, *, /)：")
        num2 = int(input("请输入一个整数："))

        if calc == '-':
            print(num1, calc, num2, "=", num1 - num2)
        elif calc == "+":
            print(num1, calc, num2, "=", num1 - num2)
        elif calc == "*":
            print(num1, calc, num2, "=", num1 * num2)
        elif calc == "/":
            print(num1, calc, num2, "=", num1 / num2)
        else:
            print("您输入的计算符号超出我的计算能力。")
    except Exception as E:
        print(E)
        print("我只会对整数的计算，请重新输入")
