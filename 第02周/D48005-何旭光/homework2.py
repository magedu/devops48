"""
1. 请写出列表支持的所有方法及说明（例如: append 向列表末尾添加元素）

index 通过值求索引
count 求元素个数
append 在后面追加元素
extend 追加多个元素
remove 通过值来移除元素
pop 通过索引删除值或者从最后面丢弃一个值
clear 清空
sort 排序
reverse 前后反转

2. 说明列表的浅拷贝和深拷贝的区别

浅拷贝只复制一层，剩下的都是地址
深拷贝可以拷贝多层

3. 说明列表和元组的相同点和不同点

相同点：
都是有序的，都可以通过索引查询遍历
不同点：
数组可变，元组不可变


4. 请写出字符串支持的所有方法及说明（例如: lower 返回字符串的小写）

join 连接字符串
find 查找子串
rfind 右查询子串
index 索引子串
count 求子串数量
split 指定分隔符分割
rsplit 右分割
splitlines 按行分割
replace 替换子串
strip 删除两边子串字符
lstrip 删除左边
rstrip 删除右边
endswith 判断是否以子串结尾
startswith 判断是否以子串开头
upper 转换大写
lower  转小写
swapcase 交换大小写
isalnum 判断是否是字母和数字组成
isalpha 判断是否全为字母
isdigit 判断是否权威数字
islower 判断是否都是小写
isupper 判断是否全为大写
isspace 盘点是否全为空白
"""

"""
5. 有如下一个字符串变量logs，请统计出每种请求类型的数量（提示：空格分割的第2列是请求类型），得到如下输出：
POST 2
GET 3

下边是logs变量：
logs = '''
111.30.144.7 "POST /mock/login/?t=GET HTTP/1.1" 200 
111.30.144.7 "Get /mock/users/?t=POST HTTP/1.1" 200 
111.13.100.92 "Post /mock/login/ HTTP/1.1" 200 
223.88.60.88 "GET /mock/users/?t=POST HTTP/1.1" 200 
111.30.144.7 "GET /mock/users/ HTTP/1.1" 200 
'''
"""

logs = """
111.30.144.7 "POST /mock/login/?t=GET HTTP/1.1" 200 
111.30.144.7 "Get /mock/users/?t=POST HTTP/1.1" 200 
111.13.100.92 "Post /mock/login/ HTTP/1.1" 200 
223.88.60.88 "GET /mock/users/?t=POST HTTP/1.1" 200 
111.30.144.7 "GET /mock/users/ HTTP/1.1" 200
"""
res = dict()
for i in logs.splitlines():
    if i.strip() == "":
        continue
    new = [j.strip().strip('"') for j in i.split(" ")]
    if len(new) < 2 or new[1] == "":
        continue
    if res.get(new[1].upper(), False):
        res[new[1].upper()] += 1
    else:
        res[new[1].upper()] = 1
for kv in res.items():
    print("{:<4} {:>2}".format(kv[0], kv[1]))
