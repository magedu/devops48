
"""
1. 请写出列表支持的所有方法及说明（例如: append 向列表末尾添加元素）
    list.index(value[, start_pos[, end_pos ]]) --> 查询vaule在list[start_pos, end_pos]中的首个索引值，匹配不到返回ValueError
    list.count(value) --> 返回list中value的个数
    len(list) --> 返回列表的长度
    list.append(object) --> 将object追加至原列表末尾，返回None
    list.insert(index, object) 在原列表的index位置追加object，返回None
    list.extend(iterable) --> 将可迭代对象追加至原列表末尾，返回None
    list.remove(value) --> 从列表中删除value，返回None，否则返回ValueError
    list.pop(index) --> index缺省值-1，删除list[index]的值，返回list[index]
    list.clear() --> 清空list
    list.reverse() --> 将list的元素反转，返回None
    list.sort(key=None, reverse=False) -->按照Key排序
    sep.join(list) --> str 将
2. 说明列表的浅拷贝和深拷贝的区别
    浅拷贝：针对引用数据类型只复制地址；
    深拷贝：针对引用数据类型会递归复制；
3. 说明列表和元组的相同点和不同点
    相同点：
        1）有序序列
        2）支持切片
    不同点：
        1）列表可变，元组不可变
4. 请写出字符串支持的所有方法及说明（例如: lower 返回字符串的小写）
    str.find(sub[, start[, end]]) -> int 在str[start, end]范围内从左往右查找sub, 没找到返回-1
    str.rfind(sub[, start[, end]]) -> int 在str[start, end]范围内从右往左查找sub, 没找到返回-1
    str.index(sub[, start[, end]]) -> int 在str[start, end]范围内从右往左查找sub, 没找到返回ValueError
    str.rindex(sub[, start[, end]]) -> int 在str[start, end]范围内从右往左查找sub, 没找到返回ValueError
    str.count(sub) -> int 统计str内的sub字符数量
    len(str) -> int 计算str的长度
    str.split(sep=None, maxsplit=-1) -> list of strings 将字符串按照sep拆分，最大拆maxsplit次（-1 表示遍历整个字符串）
    str.rsplit(sep=None, maxsplit=-1) -> list of strings 将字符串按照sep拆分自右向左，最大拆maxsplit次（-1 表示遍历整个字符串）
    str.splitlines([keepends]) -> list of strings 将字符串以换行符拆分
    str.partition(sep) -> (head, sep, tail) 从左至右，遇到分隔符就把字符串分割成两部分，返回头、分隔符、尾三部分的三元
    str.rpartition(sep) -> (head, sep, tail) 从右至左，遇到分隔符就把字符串分割成两部分，返回头、分隔符、尾三部分的三元组
    str.replace(old, new[, count]) -> str 字符串中找到匹配替换为新子串,替换count次，默认全部替换，返回新字符串
    str.strip([chars]) -> str 在字符串两端去除指定的字符集chars中的所有字符,默认空白字符
    str.lstrip([chars]) -> str ，从左开始
    str.rstrip([chars]) -> str，从右开始
    str.endswith(suffix[, start[, end]]) -> bool 在指定的区间[start, end)，字符串是否是suffix结尾
    startswith(prefix[, start[, end]]) -> bool 在指定的区间[start, end)，字符串是否是prefix开头
    str.upper()大写
    str.lower()小写
    str.swapcase() 交换大小写
    str.isalnum() -> bool 是否是字母和数字组成
    str.isalpha() -> bool 是否是字母
    str.isdecimal() -> bool 是否只包含十进制数字
    str.isdigit() -> bool 是否全部数字(0~9)
    str.isidentifier() -> bool 是不是字母和下划线开头，其他都是字母、数字、下划线
    str.islower() -> bool 是否都是小写
    str.isupper() -> bool 是否全部大写
    str.isspace() -> bool 是否只包含空白字符


5. 有如下一个字符串变量logs，请统计出每种请求类型的数量（提示：空格分割的第2列是请求类型），得到如下输出：
POST 2
GET 3

下边是logs变量：
logs = '''
111.30.144.7 "POST /mock/login/?t=GET HTTP/1.1" 200 
111.30.144.7 "Get /mock/users/?t=POST HTTP/1.1" 200 
111.13.100.92 "Post /mock/login/ HTTP/1.1" 200 
223.88.60.88 "GET /mock/users/?t=POST HTTP/1.1" 200 
111.30.144.7 "GET /mock/users/ HTTP/1.1" 200 


'''
"""

logs = '''
111.30.144.7 "POST /mock/login/?t=GET HTTP/1.1" 200 
111.30.144.7 "Get /mock/users/?t=POST HTTP/1.1" 200 
111.13.100.92 "Post /mock/login/ HTTP/1.1" 200 
223.88.60.88 "GET /mock/users/?t=POST HTTP/1.1" 200 
111.30.144.7 "GET /mock/users/ HTTP/1.1" 200 

'''
list_log = logs.splitlines()

methods = [ item.split()[1][1:].upper() for item in list_log if item ]

print(f"POST的数量是{methods.count('POST')}, GET方法的数量是{methods.count('GET')}")







