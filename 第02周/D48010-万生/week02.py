"""
1. 请写出列表支持的所有方法及说明（例如: append 向列表末尾添加元素）
a = ['p', '2', 't', (1, 2, 3), {'pual':18}]
a.index('p', 0, 5)
a.count(2)
len(a)
a.insert(4, "wan")
a.append("rui")
a.extend(range(2))
a.pop(-1) == a.pop()
a.remove("wan")
a.reverse()
a.sort(key=lambda elem: len(elem), reverse=True)
print(a)
2. 说明列表的浅拷贝和深拷贝的区别
 shadow copy 影子拷贝，也叫浅拷贝。遇到引用类型数据，仅仅复制一个引用而已
 deep copy 深拷贝，往往会递归复制一定深度
一般情况下，大多数语言提供的默认复制行为都是浅拷贝。
3. 说明列表和元组的相同点和不同点
相同点：有序，元素可以重复，都可以存放不同类型的数据
不同点：list可修改，tuple不可修改
元组缓存于 Python 运行时环境，一旦生成，每次调用不需要重新分配内存
4. 请写出字符串支持的所有方法及说明（例如: lower 返回字符串的小写）
a = 'string'
print(a.upper())
b = '-'.join(a)
a.find('s',2,4)
a.count('s',0,5)
a.index('s',0,5)
print(b.split('-'))
print(b.split('r'))
print(b.splitlines())
c = ','.join(a)
print(c.partition('r'))
print(b.replace('-', '*', 3))
print(c.strip('r,i'))
5. 有如下一个字符串变量logs，请统计出每种请求类型的数量（提示：空格分割的第2列是请求类型），得到如下输出：
POST 2
GET 3
下边是logs变量：
logs = ""
111.30.144.7 "POST /mock/login/?t=GET HTTP/1.1" 200 
111.30.144.7 "Get /mock/users/?t=POST HTTP/1.1" 200 
111.13.100.92 "Post /mock/login/ HTTP/1.1" 200 
223.88.60.88 "GET /mock/users/?t=POST HTTP/1.1" 200 
111.30.144.7 "GET /mock/users/ HTTP/1.1" 200
""
words = dict()
for line in logs.strip().splitlines():
    filed = line.split()
    method = filed[1]
    method = method.replace('"', '').upper()
    if method in words:
        words[method] += 1
    else:
        words[method] = 1
print(words)

