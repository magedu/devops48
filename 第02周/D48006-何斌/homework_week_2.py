"""
请写出列表支持的所有方法及说明
"""
a = [1, 2, 3, 4]
a.append(5) #列表末尾添加元素，就地修改，无输出，返回List
print(a)

a.clear()   #清空列表内所有元素，返回列表（列表内所有元素引用计数减1）
print(a)

a.copy  #浅拷贝，b为a的副本，实际只拷贝了list a内的内存地址，若需实现深拷贝需使用copy模块copy.deepcopy()实现
a = [1, 2, ['a', 'b', 'c', 3]]
b = a
a[0] = 100
print(b)
a[2][0] = 100
print(b)    #浅拷贝
import  copy
a = [1, 2, ['a', 'b', 'c', 3]]
b = copy.deepcopy(a)    #深拷贝,递归拷贝
a[2][0] = 100
print(a)
print(b) 


a.count     #统计列表内元素的个数，时间复杂度O（n）
a = [1, 2, 1, 1, 3, 1, 2]
print(a.count(1))

a = [1, 2, 3]
a.extend    #列表末尾添加可迭代对象,返回None,返回类型list,只支持一个参数：TypeError: list.extend() takes exactly one argument (2 given)
print(a.extend(range(3)))   
print(a)

a.index     #返回指定元素在列表中出现的第一个索引,若不存在则抛出异常，a.index(value, start=0, stop=9223372036854775807, /)
a = ['a', 'b', 'c', 'd']
print(a.index('c', 1, 3))

a = ['a', 'b', 'c', 'd']
a.insert    #在指定索引前插入对象，a.insert(index, object, /),返回None
print(a.insert(0, 'e')) #返回值为None
print(a)

a = ['a', 'b', 'c', 'd']
a.pop       #根据给定索引移除并返回一个元素，默认从索引-1开始，Signature: a.pop(index=-1, /)，若list为空或索引超界则抛出异常
print(a.pop(0)) #返回'a'，返回类型视被移除元素类型而定

a.remove    #移除列表中匹配到的第一个元素，若元素不存在则报ValueError,返回None
#特例
a = [1, 2, 3, True]  #True等价于1，False等价于0，需注意
a.remove(True)
print(a)

a = ['a', 'b', 'c', 'd']
a.reverse   #将列表反转，推荐使用reversed()函数,返回None
print(a.reverse())
print(a)

a = ['a', 'b', 'c', 'd']
a.sort      #默认以升序（reverse=False）对列表进行排序，Signature: a.sort(*, key=None, reverse=False)，key表示可指定进行比较的参数，会对原有
#示例：      #列表进行修改，排序推荐使用sorted()函数
def takeSecond(elem):
    return(elem[1])

a = [('a', 2), ('b', 1), ('c', 3), ('d', 4)]
a.sort(key=takeSecond)
print(a)

#[('b', 1), ('a', 2), ('c', 3), ('d', 4)]

# a = [('a', 2), ('b', 1), ('c', 3), ('d', 4)]
# a.sort(key=a[1])
# print(a)
#TypeError: 'tuple' object is not callable  元组不可被调用


#说明列表和元组的相同的和不同点
"""
相同点：
1、都属于有序序列，支持使用双向索引访问元素
2、都支持使用len()函数统计长度
3、都支持使用count和index方法
4、都支持切片操作

不同点：
1、列表可变而元组不可变
2、列表支持增删改操作而元组不支持
3、元组可哈希而列表不可哈希
4、元组访问速度比列表更快
"""


#请写出字符串支持的所有方法及说明
a = 'www.magedu.com'

a.capitalize    #返回一个首字母大写的字符串，返回一个新的字符串
print(a.capitalize())
print(a)

a.casefold      #将字符串中的字符全部转换为小写并进行Unicode规范化，返回一个新的字符串
a = 'WWW.123.Com'
print(a.casefold())

a = 'www.magedu.com'
b = a.center(20, "-")        #将字符串居中并以指定字符填充两边，Signature: a.center(width, fillchar=' ', /),width指定宽度必须大于字符串长度
print(b)

a = 'www.magedu.com'
a.count('w')    #统计给定元素在字符串中出现的次数，O(n)
a.encode(encoding='GBK', errors='strict')        #使用指定编码方式对字符串进行编码，a.encode(encoding='utf-8', errors='strict')，编码方式有多种，errors可指定 'ignore', 'replace', 'xmlcharrefreplace', 'backslashreplace'
a.endswith('m', 2, -1)  #查看字符串是否以指定字符结束并返回True或False，可使用索引指定区间

a = 'w  ww.magedu.com'
a.expandtabs()           #Signature: a.expandtabs(tabsize=8)把字符串中的tab转换为空格，默认每个tab转换为8个空格

a = 'www.magedu.com'
print(a.find('e', 3, -1))   #查询一个子串在指定区间内出现的第一个索引位置,若子串不存在或不在区间内则返回-1不抛出异常
print(a.find('ma', 2, 7))
print(a.find('mage', 8, -1))
print(a.find('x'))



a.format    #格式化字符串

a = 'www.magedu.com'
a.format_map
"""
该方法类似于 str.format(**mapping) 都可以进行字符串格式化，不同之处在于 format( ) 方法是一种所有情况都能使用的格式化方法，
而 str.format_map(mapping) 方法仅适用于字符串格式中可变数据参数来源于字典等映射关系数据时。mapping 会被直接使用而不是复制到一个 dict
"""


weather = "晴天"
hours = 7
s = "今天天气是{weather}，现在时间是{hours}点整"
print(s.format_map(vars()))

a = '{who} am {action} python, {days}!'
dict_b = {'who':'I', 'action':'learning', 'days':10}
print(a.format_map(dict_b))

dict_c =  {'who':'I', 'action':'learning', 'days':10, 'where':'home'} #字典中的映射关系大于需格式化区域时正常运行，反之报错
print(a.format_map(dict_c))

dict_d = {'who':'I', 'action':'learning'}
print(a.format_map(dict_d))


a = 'www.magedu.com'
a.index #返回给定字符在字符串中第一次出现的索引位置，若值不存在则报异常
print(a.index('m'))

a.isalnum   #若字符串至少有一个字符并且所有字符都是字母或数字则返回True，反之返回false
a.isalpha   #判断字符串是否都是字母
a.isdecimal #判断字符串是否只包含十进制数
a.isascii   #判断字符串是否全是ascii码
a.isdigit   #判断字符串是否全是数字
a.isidentifier  #判断字符串是否是有效的python标识符，可用来判断变量名是否合法
a.islower   # 判断字符串中字符是否只有小写
a = 'WWW.magedu123.com'
b = 'www.magedu.com'
c = 'wwwmageducom'
d = 'abc123'
e = 'abc.123'
print(a.islower())
print(b.islower())
print(c.islower())
print(d.islower())
print(e.islower())

a.isnumeric     #如果字符串中只包含数字字符则返回True否则返回False
a = '123'
b = 'abc234'
c = '123.'
print(a.isnumeric())
print(b.isnumeric())
print(c.isnumeric())

a.isprintable   #判断字符串中所有字符是否都是可打印字符或字符串为空
print('abc'.isprintable())
print('abc\td'.isprintable())
print('abc\nd'.isprintable())
print('abc\rd'.isprintable())
print(' '.isprintable())
print(''.isprintable())

a.isspace   #判断字符串是否只包含空格
print('a bcd'.isspace())
print('abc'.isspace())
print(' '.isspace())
print('\t\t\n'.isspace())

a.istitle   #判断字符串是否是标题化的（每个单词首字母大写）

a.isupper   #判断字符串中字符是否全大写
print('ABC'.isupper())
print('Abc'.isupper())
print('ABC123'.isupper())
print('ABC.DEF'.isupper())
print('A\tBD'.isupper())

a.join  #以指定字符拼接字符序列
a = ['www', 'magedu', 'com']
print('.'.join(a))

a = 'www.magedu.com'
a.ljust     #Signature: a.ljust(width, fillchar=' ', /),返回宽度为width的字符串，原字符串在其中靠左对齐，使用 指定字符填充，默认空格
            #若宽度小于等于原字符串则返回原字符串,str.rjust()右对齐
a.ljust(8)
a.ljust(20, '-')


a = 'www.magedu.com'
a.removeprefix('w')  #从左至右移除匹配到的第一个指定元素，移除前缀，对应str.lstrip（该方法会移除匹配到的所有元素）
a.lstrip('w')          
a.removesuffix('m')
a.rstrip('m')

a = 'www.\tmagedu.      \rco\nm  \nm'
a.strip('wm')
#'.\tmagedu.      \rco\nm  \n'  移除字符串头尾匹配到的指定元素和空格，返回一个新的字符串，掐头去尾

a = 'www.magedu.com'
a.lower #将字符串内字母转换成小写
a.upper #将字符串内字符转换成大写

a = 'www.magedu.com'
a.partition('.')
#('www', '.', 'magedu.com') 以指定分割符对字符串进行切割从左至右，返回一个三元组(注意：返回类型为元组)
a.partition('w')
#('', 'w', 'ww.magedu.com')
a.partition('-')
#('www.magedu.com', '', '')  没切到也返回三元组

a = 'www.magedu.com'
a.split


"""
有如下一个字符串变量logs，请统计出每种请求类型的数量（提示：空格分割的第2列是请求类型），得到如下输出：
POST 2
GET 3

下边是logs变量：
logs = '''
111.30.144.7 "POST /mock/login/?t=GET HTTP/1.1" 200 
111.30.144.7 "Get /mock/users/?t=POST HTTP/1.1" 200 
111.13.100.92 "Post /mock/login/ HTTP/1.1" 200 
223.88.60.88 "GET /mock/users/?t=POST HTTP/1.1" 200 
111.30.144.7 "GET /mock/users/ HTTP/1.1" 200
"""


logs = """
111.30.144.7 "POST /mock/login/?t=GET HTTP/1.1" 200 
111.30.144.7 "Get /mock/users/?t=POST HTTP/1.1" 200 
111.13.100.92 "Post /mock/login/ HTTP/1.1" 200 
223.88.60.88 "GET /mock/users/?t=POST HTTP/1.1" 200 
111.30.144.7 "GET /mock/users/ HTTP/1.1" 200
"""

log_1 = logs.strip().upper().splitlines()
print(log_1)
post = 0
get = 0
for i in range(len(log_1)):
    if log_1[i].find('"POST', 0, 19) > 0:
        post += 1
    elif log_1[i].find('"GET', 0, 19) > 0:
        get += 1
print('POST:{}'.format(post))
print('GET:{}'.format(get))

print('-'*30)

log_2 = logs.strip().lower()
post = log_2.count('"post')
get = log_2.count('"get')
print('POST {}'.format(post))
print('GET {}'.format(get))


print('-'*30)


logs = """
111.30.144.7 "POST /mock/login/?t=GET HTTP/1.1" 200 
111.30.144.7 "Get /mock/users/?t=POST HTTP/1.1" 200 
111.13.100.92 "Post /mock/login/ HTTP/1.1" 200 
223.88.60.88 "GET /mock/users/?t=POST HTTP/1.1" 200 
111.30.144.7 "GET /mock/users/ HTTP/1.1" 200
"""


log_3 = logs.strip().upper().splitlines()   #获取五元素的列表
print(log_3)

p = 0
g = 0
for i in range(len(log_3)):
    if log_3[i].split()[1].strip('"') == 'POST':
        p += 1
    elif log_3[i].split()[1].strip('"') == 'GET':
        g += 1
print('POST {}'.format(p))
print('GET {}'.format(g))



