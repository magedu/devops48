完成作业：
1. 请写出列表支持的所有方法及说明（例如: append 向列表末尾添加元素）
	list:
		添加：
			insert 指定索引处添加插入元素，insert(index,value)
			append 向列表末尾添加元素
			extend 将可迭代对象追加进来 
		删除：
			pop 	指定索引删除并返回该元素，不指定默认删除最后一个元素
			clear 	清空所有元素，留下空列表
			remove  从左至右查找第一个匹配value的值，移除
			del 	指定索引删除该元素，del list[index]
		修改：
			list[index] = new_value 修改指定元素
			reverse     将列表元素反转，修改原列表
			sort        对列表进行排序，修改原列表；list.sort(key=None,reverse=Flase);key=function指定如何排序，默认为空；reverse=True为降序
			'+ *' 	    支持运算符拼接列表，或者乘倍数添加列表元素
		查询：
			index 	输入元素，查询到返回下标，查询不到报错ValueError；list.index(value)
			in 	判断元素是否在列表里，存在返回True，value in list
			count 	查询指定元素在列表的数量，返回总数。list.count(value)
			len 	返回列表的长度；len(list)
		切片操作：
			list[start:stop:step]	
		    	#start 起始位置；stop 结束位置，不包括结束下标；step 步长，指定每次间隔多长切一次 
			--->	list[:::]			#不指定返回全部
			--->	list[-1::-1] or list[:0:-1]	#step = -1,从右往左切片，起始位置和结束位置需要从右往左
			
2. 说明列表的浅拷贝和深拷贝的区别
	list1 = [[1]，2，3，4]
	浅拷贝：	#以嵌套列表为例，浅拷贝是影子拷贝，列表内容相同，内存位置不同；列表内部的列表没有重新复制一份新的，而是在内存中直接指向原列表中的该嵌套列表，因此，原列表嵌套列表变化，新列表的嵌套列表也会变化
		list2 = list1
		list1[0][0] = 1000
		list1[1] = 100
		print(list1,list2)	    #会发现list2[1]仍然是2，但是list2[0][0]索引位置也变成了1000
	深拷贝：	#copy模块提供了deepcopy，深拷贝会对原列表的每个元素都进行完整复制（包括原列表内的嵌套列表里面的元素）。原列表数据发生改变，新列表不会发生改变
		list3 = list1
		list1[0][0] = 1000
		list1[1] = 100
		print(list1,list3)	#会发现list3的数据没有发生变化
		
3. 说明列表和元组的相同点和不同点
	相同点：
		1.都支持切片操作
		2.都是可迭代元素
		3.都是有序的线性数据结构
	不同点：
		1.列表是可变的；元组本身不可变，只有内部有可变元素时，可变元素可变
		2.列表支持增删改查，元组只支持查看
		3.元组只有一个元素时，需要在元素后面加个','，否则不是元组
		
4. 请写出字符串支持的所有方法及说明（例如: lower 返回字符串的小写）
	常用：	
		'+'		#字符串拼接
		'*'		#字符串乘法
		count		#统计指定字符个数，string.count(指定字符,start,stop),不指定起始位置默认查询整个字串
		casefold / lower()	#全部变小写，string.casefold() / string.lower()
		upper		#全大写，string.upper()
		capitalize	#首字母大写，string.capitalize()
		swapcase	#交互大大小写，string.swapcase()
	查找：
		find	#在指定区间从左至右查找子串sub。找到返回索引，没找到返回-1，string.find(sub[start,stop])
		rfind	#在指定的区间从右至左查找子串sub。找到返回索引，没找到返回-1，string.rfind(sub[start,stop])
		index	#同find用法一致，但是找不到抛出异常ValueError
		rindex	#同rfind用法一致，但是找不到抛出异常ValueError
	修改：
		replace	#字符串中找到匹配到的子串替换为新子串，返回新子串，不指定count默认替换全部符合的子串，string.replace(old,new,count)
		strip	#从字符串两端去除指定的字符集chars中的所有字符,无指定则去除两端的空白字符，string.strip([chars])
	处理：
		join	#可以指定字符与字符之间的分割符，引号中设置分割符，括号中可以是字符串，也可以是一个可迭代对象；"".join(self/iterable)
		split	#将字符串按照分隔符分割成若干字符串，并返回列表。sep指定分割字符串，默认为空白字符串;maxsplit指定分割次数，默认遍历整个字符串。string.split(sep=None,maxsplit=-1)
	格式化：
		format	#将字符串按照指定格式打印。可以在字符串里加入占位符，随后再填充进字符串当中，
			#"{}{xxx}".format(*args,**kwargs)，args是一个位置参数；kwargs是一个关键字参数；花括号是占位符，可以用位置参数占位，也可以用关键字参数占位。

	
5. 有如下一个字符串变量logs，请统计出每种请求类型的数量（提示：空格分割的第2列是请求类型），得到如下输出：
POST 2
GET 3

下边是logs变量：
logs = '''
111.30.144.7 "POST /mock/login/?t=GET HTTP/1.1" 200 
111.30.144.7 "GET /mock/users/?t=POST HTTP/1.1" 200 
111.13.100.92 "Post /mock/login/ HTTP/1.1" 200 
223.88.60.88 "GET /mock/users/?t=POST HTTP/1.1" 200 
111.30.144.7 "GET /mock/users/ HTTP/1.1" 200 
'''


#第一种方法：
min_logs = logs.casefold().split("\n")
post_sum, get_sum = 0, 0
for i in min_logs:
    if i.find('"post') != -1:
        post_sum += 1
    elif i.find('"get') != -1:
        get_sum += 1
    else:
        pass
print(f"POST {post_sum}\nGET {get_sum}")


#第二种方法：
min_logs = logs.casefold()
print("POST {}".format(min_logs.count("\"post")))
print("GET {}".format(min_logs.count("\"get")))




















