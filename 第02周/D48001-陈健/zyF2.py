# 1、列表常用方法：
# index：通过值查询索引下标，返回匹配到的第一个值的索引，匹配不到抛异常 ValueError
# count:返回匹配值在对应类别中出现的次数
# len：返回列表内元素个数
# 追加：
# append：尾部追加 返回值为空None，就地直接修改表
# insert：通过索引进行指定位置添加，返回值为None，导致列表内的所有元素挪动，可以超界实现尾部或者头部追加
# extend：通过可迭代对象增加多个元素，返回值为空None
# 删除：
# remove：通过值进行查找然后删除从左到右匹配到的第一个值，和index相似
# pop：通过索引进行查找，不指定索引就从尾部溢出，指定索引就从指定位置操作
# clear：清空列表所有元素，仅剩空列表
# 排序：
#   reverse：反转，返回空，推荐使用reversed函数实现而不通过方法直接处理
#   reversed函数并不能够支持所有可迭代变量，只能反着读取有序序列
# sort：排序，通过key可以指定函数，通过函数进行判断后进行排序，reverse通过 true false决定是降序还是正常排序从大到小，true为降序
# sorted函数：排序会返回一个新的列表，sort不会
# 复制：
#  copy：制作一个副本，需要考虑引用地址的观念，字面常量直接存在表中，非字面常量就都是引用的内存地址
#
# 2、浅拷贝和深拷贝的区别
# 浅拷贝原封不动的复制，引用地址也还是引用地址，深拷贝会将对象的所有元素都制作一份副本，即使对应元素是列表和元祖也会重新复制生成一份而
# 不是使用的对像的引用地址
#
# 3、列表和元组的区别
# 相同：列表和元祖都是有序序列，使用方法和列表相同
# 不同：列表元素可变，元祖只读不可变，定义的时候已经锁死了，不能增加删除和修改，同样的元素元祖比列表占用的内存空间更小，可以通过
# 元祖里套列表修改列表元素，修改的都是应用地址对应的数据
#
# 4、字符串的常用方法
# 查找：和列表使用方法一样
# index：通过值查索引，从左到右，会抛异常
# rindex：从右到左，也还是返回正索引
# count：统计值出现的次数
# len：返回字符串长度
# find：在指定区间内查找后返回正索引，没找到返回-1，从左至右前包后不包
# rfind：从右至左
# 分割：
# split:从左至右，缺省默认使用空白字符串作为分隔符，连续的空白字符会被当做一次分割，
#   指定的分割字符串如果不被包含在字符串中就返回存放字符串本身的列表，maxsplit可指定分割次数，-1代表遍历整个字符串，会立即返回分割后的列表
# rsplit：从右至左，但是输出的字符串字符不会烦，
# splitlines：按照行进行分割，通过keepends确定是否保留hang分割符，默认为false不保留
# partition：从左至右切个为三段，头部 分隔符和尾部，找不到就返回字符串本身和两个空元素组成的元祖
# rpartition：从右至左
# 替换：
# replace：将对应old字符串替换为new字符串，通过count指定替换几次，不指定就全部替换
# 移除：
# strip：在字符串两端去除指定字符集合中的所有字符，字符集合，不分先后顺序，只要属于集合内的都会处理，前后端粉笔遇到第一个不在这个集合内的就停止
#    不指定chars时去除空白字符
# lstrip：从左开始
# rstrip：从右开始
# 判断：
# endswith：从指定区间开始判断字符串是否是以对应字符串结尾
# startwith:自ID哪个区将字符串是否是以对应字符串开头
# 转换及判断：
# upper：所有字幕转换为大写返回新字符串
# lower：所有字母转换为小写，返回新字符串
# swapcase：所有字幕大小写互换
# isalnum：判断是否由数字和字母组成
# isalpha：判断是否由字母注册
# isalcimal：判断是否只包含十进制数字
# isdigit：判断是否只包含数字
# isidentifier：判断是否为合法的标识符（变量名）
# islower：判断所有字母是否为小写
# isupper：是否为大写
# isspace：是否只包含空白字符
#
logs = '''
111.30.144.7 "POST /mock/login/?t=GET HTTP/1.1" 200 
111.30.144.7 "Get /mock/users/?t=POST HTTP/1.1" 200 
111.13.100.92 "Post /mock/login/ HTTP/1.1" 200 
223.88.60.88 "GET /mock/users/?t=POST HTTP/1.1" 200 
111.30.144.7 "GET /mock/users/ HTTP/1.1" 200 
'''
l2 = logs.strip().split("\n")
p = 0
g = 0
end_num = l2[0].find("/")
for i in range(5):
    if l2[i].find("P", 0, 19) > 0:
        p += 1
    elif l2[i].find("G", 0, 19) > 0:
        g += 1
print("Post {}".format(p))
print("Get {}".format(g))
