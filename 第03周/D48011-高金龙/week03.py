1. 如何为函数定义keyword-only参数（写出个例子即可）？
def kw_fuc(**kwargs):
	print(kwargs)

2. 什么是LEGB，请解释
L：local本地作用域，函数调用时创建，调用结束时消亡
E：enclosing，python2.2之后引入了嵌套函数，实现了闭包，enclosing是嵌套函数的外部函数的命名空间
G：global，全局作用域，模块的命名空间，模块被import引用模块时创建，解释器退出时消亡
B：build-in，python函数内置模块的命名空间，解释器启动时创建，解释器退出时消亡

4. 使用本周学习的技术改造第一周的计算器实现，其目标如下：

   1. 运行后提示让用户输入一个数字
   2. 提示输入操作符（+ - * /）
   3. 再次提示输入一个数字
   4. 打印计算结果
   5. 在不退出程序的前提下，可以允许用户继续输入新一组数据计
   6. 尽可能改善用户体验（新需求）


def calculate(one_num=""):
    one_num = input("请输入一个数字：")
    exit_calculat = "退出计算器，感谢本次使用"

    #退出指令
    if one_num == 'exit' or one_num == "":
        print(exit_calculat)
        return None
    elif one_num.isdigit() is False:    #判断是否为数字
        print("输入有误，请重新输入：")
        return calculate(one_num)

    def compute(choice_symbol=''):
        if choice_symbol == '':
            choice_symbol = input("请输入操作符：+ 、- 、* 、/ ")
            if choice_symbol not in ["+","-","*","/"]:      #判断操作符是否正确
                print("输入有误，请重新输入：")
                return compute(choice_symbol="")
            else:
                two_num = input("请输入另一个数字：")
                #判断是否为数字
                if two_num.isdigit() is False:
                    print("输入有误，请重新输入：")
                    return compute(choice_symbol)
                #计算
                if choice_symbol == "+":
                    print("{0} + {1} = {2:.2f}".format(one_num, two_num, float(one_num) + float(two_num)))
                elif choice_symbol == "-":
                    print("{0} - {1} = {2:.2f}".format(one_num, two_num, float(one_num) - float(two_num)))
                elif choice_symbol == "*":
                    print("{0} * {1} = {2:.2f}".format(one_num, two_num, float(one_num) * float(two_num)))
                elif choice_symbol == "/":
                    print("{0} / {1} = {2:.2f}".format(one_num, two_num, float(one_num) / float(two_num)))

    compute()
    return calculate(one_num)

calculate()
