# 1. 如何为函数定义keyword-only参数（写出个例子即可）？
def foo(*, kw1, kw2):
    pass

# 2. 什么是LEGB，请解释
"""     L：local 本地变量
        E: Enclosing 外层函数，局部变量
        G: Global 模块有关，当前模块中的全局。
        B: Bulit-in print, str, list, int, range 生命周期和解释器 相同
        变量解析的顺序L->E->G->B
        """
"""
4. 使用本周学习的技术改造第一周的计算器实现，其目标如下：
   1. 运行后提示让用户输入一个数字
   2. 提示输入操作符（+ - * /）
   3. 再次提示输入一个数字
   4. 打印计算结果
   5. 在不退出程序的前提下，可以允许用户继续输入新一组数据计
   6. 尽可能改善用户体验（新需求）
"""

def input_num():
    num = input("请输入一个整数：")
    if not num.isdigit():
        if num.upper() == 'Q':
            print('欢迎再次光临。')
            exit()
        return input_num()
    return int(num)

def input_operator():
    oper = input("请输入操作符：")
    if oper not in {'+', '-', '*', '/'}:
        if oper.upper() == 'Q':
            print('欢迎再次光临')
            exit()
        return input_operator()
    return oper

def calc(num1:int, num2:int, operator:str) -> int:

        if operator == '+':
            return num1 + num2
        elif operator == '-':
            return num1 - num2
        elif operator == '*':
            return num1 * num2
        else:
            if num2 == 0:
                return '被除数不允许为0.'
            return round(num1 / num2, 2)


if __name__ == '__main__':
    while True:
        num1 = input_num()
        operator = input_operator()
        num2 = input_num()
        res = calc(num1, num2, operator)
        print(res)

