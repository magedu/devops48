1. 内置的open函数打开文件有几种模式，它们的区别是什么？
内置的 `open()` 函数打开文件时，可以指定不同的模式参数来控制文件的打开方式。常用的模式有以下几种：

1. **"r"（默认模式）**：只读模式。以文本形式打开文件，并允许读取文件内容。如果文件不存在，会抛出 `FileNotFoundError` 错误。

2. **"w"**：写入模式。以文本形式打开文件，并清空文件内容。如果文件不存在，则创建一个新文件。如果文件已存在，则覆盖原有内容。

3. **"a"**：追加模式。以文本形式打开文件，并将数据追加到文件末尾。如果文件不存在，则创建一个新文件。

4. **"x"**：独占创建模式。以文本形式创建文件，但只在文件不存在时才成功，否则会抛出 `FileExistsError` 错误。

5. **"b"**：二进制模式。以二进制形式打开文件，适用于非文本文件（如图像、音频等）。与其他模式结合使用，如 `"rb"`（二进制读取）、`"wb"`（二进制写入）等。

此外，还可以结合上述模式参数添加其他字符来提供更多功能：

- **"+"**：读写模式。为了同时支持读取和写入操作，我们可以在模式参数中添加 "+" 符号，如 `"r+"`（读写模式），`"w+"`（读写模式，清空文件内容），`"a+"`（读写模式，追加到文件末尾）。

- **"t"**：文本模式（默认）。与其他模式结合使用，如 `"rt"`（文本读取）、`"wt"`（文本写入）等。在最新版本的 Python 中，指定 "t" 是可选的，默认会以文本形式打开文件。

综上所述，内置的 `open()` 函数提供了多种模式参数来满足不同的文件操作需求，包括只读、写入、追加、独占创建和二进制操作等，并且可以结合其他字符提供更多功能（如读写模式、文本模式）。选择合适的模式取决于你的具体需求。
2. 使用base64解码“bWFnZWR1LmNvbQ==”，使用base64编码”[magedu.com](http://magedu.com)”，分别给出它们的解码和编码结果。

import base64

# 解码
encoded_str = "bWFnZWR1LmNvbQ=="
decoded_str = base64.b64decode(encoded_str).decode('utf-8')
print("解码结果：", decoded_str)

# 编码
text_str = "[magedu.com](http://magedu.com)"
encoded_str = base64.b64encode(text_str.encode('utf-8')).decode('utf-8')
print("编码结果：", encoded_str)


3. 列出本周讲的几种序列化方法，它们各自的特点是什么?
在Python中，常用的序列化方法有以下几种：

1. **JSON（JavaScript Object Notation）**：JSON是一种轻量级的数据交换格式，易于人类阅读和编写，并且方便在不同编程语言之间进行解析和生成。特点如下：
   - 格式简洁，易于理解和阅读。
   - 支持基本数据类型（如字符串、数字、布尔值）以及列表、字典等复杂数据结构。
   - 在不同编程语言中广泛支持。
   - 不支持存储函数、类和实例等特殊对象。

2. **Pickle**：Pickle 是 Python 的内置模块，用于对象的序列化和反序列化。它可以将 Python 对象转换为二进制数据，并在需要时恢复为原始对象。特点如下：
   - 支持序列化几乎所有的 Python 数据类型，包括自定义类和实例。
   - 序列化后的数据是二进制形式的，不易阅读和编写。
   - 仅能在 Python 中使用，不适用于不同编程语言之间的数据交换。

3. **YAML（YAML Ain't Markup Language）**：YAML 是一种人类友好的数据序列化格式，旨在提供易读性和可编辑性。它可以表示各种数据类型和数据结构，并支持注释。特点如下：
   - 格式简洁、易读。
   - 支持基本数据类型和复杂数据结构，如列表、字典等。
   - 与多种编程语言兼容，且支持跨语言的数据交换。
   - 不支持存储函数和类。

4. **MessagePack**：MessagePack 是一种二进制的、高效的数据序列化格式，类似于 JSON，但更紧凑和快速。特点如下：
   - 序列化后的数据是二进制形式的，不易阅读和编写。
   - 支持基本数据类型和复杂数据结构。
   - 提供多种语言的实现，可用于不同编程语言之间的数据交换。
   - 不支持存储函数和类。

每种序列化方法都有其适用的场景和特点，在选择时需要根据实际需求进行评估。例如，JSON适合在不同平台之间传输和交换数据；Pickle适合在同一平台内保留对象状态；YAML适合配置文件和人类可读的数据交换；MessagePack适合对性能要求较高的应用场景。
4. 写一个Python脚本实现找到/tmp目录及其子目录下的以.htm结尾的文件，把其后缀名改为.html"""
import os
import shutil

def change_extension(directory):
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith('.htm'):
                file_path = os.path.join(root, file)
                new_file_path = os.path.splitext(file_path)[0] + '.html'
                shutil.move(file_path, new_file_path)
                print(f"将文件 {file_path} 的后缀名改为 .html")

# 指定目录
target_directory = '/tmp'

# 调用函数进行后缀名修改
change_extension(target_directory)

