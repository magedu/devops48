"""
第11节，文件IO和序列化（二）
csv文件处理
树
正则表达式1
正则表达式2
复制作业详解
正则习题讲解1
正则习题讲解2
re模块使用
argparse模块使用
第12节，面向对象
面向对象概念
类、实例、属性、self
属性本质和属性访问规则
方法、类方法、静态方法原理
访问控制

完成作业：

1.
内置的open函数打开文件有几种模式，它们的区别是什么？
    r - - read only 只读打开，如不存在则报错
    w -- Write 只写打开，如存在清空内容，如不存在则创建
    a -- append 只写打开，如存在，在后方追加内容，如不存在则创建
    x -- 只写开发，如存在则报错，不存在则创建
    b -- 二进制模式打开
    + -- 增强模式打开

2.
使用base64解码“bWFnZWR1LmNvbQ ==”，使用base64编码”[magedu.com](http: // magedu.com)”，分别给出它们的解码和编码结果。
3.
列出本周讲的几种序列化方法，它们各自的特点是什么?
    pickle: Python程序之间可以都用pickle，解决所有Python类型数据的序列化，但是如果跨平台，跨语言，跨协议pickle就不太适合了
    json：最通用的序列化与反序列化，类比python中的字典，很简单
    MessagePack： 基于二进制高效的对象序列化，比json更快速
4.
写一个Python脚本实现找到 / tmp目录及其子目录下的以.htm结尾的文件，把其后缀名改为.html
from pathlib import Path
from os import rename

p = Path(r'D:\a')


def change_suffix(path:Path):
    for i in path.iterdir():
        if i.is_dir():
            change_suffix(i)
            print(i)
        else:
            if i.suffix == '.htm':
                rename(i, i.with_suffix('.html'))


change_suffix(p)
"""

