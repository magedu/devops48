"""
1. 内置的open函数打开文件有几种模式，它们的区别是什么？

"r"（只读模式）：这是默认的模式，它表示打开文件后只能进行读取操作，而不能进行写入或修改操作。如果文件不存在，会引发FileNotFoundError异常。
"w"（写入模式）：在这种模式下，可以写入或覆盖文件的内容。如果文件不存在，会创建一个新文件；如果文件已经存在，会清空文件的内容。
"a"（追加模式）：在这种模式下，可以在文件的末尾追加新的内容，而不覆盖原有的内容。如果文件不存在，会创建一个新文件。
"x"（独占模式）：这种模式用于创建一个新文件，如果文件已经存在，会引发FileExistsError异常。
"b"（二进制模式）：这种模式用于读取或写入二进制数据，例如文件中的图像、音频、视频等。
"t"（文本模式）：这是默认的模式，它用于读取或写入文本数据。
"U"（行末换行模式）：这种模式会将每一行的结尾换行符去掉，使得每一行的结尾都是一个完整的字符串。
这些模式可以通过多种组合使用，例如"wa"是追加模式，"wb"是二进制追加模式。在打开文件时，应根据需要选择合适的模式。

2. 使用base64解码“bWFnZWR1LmNvbQ==”，使用base64编码”[magedu.com](http://magedu.com)”，分别给出它们的解码和编码结果。
"""
# 解码

import base64
import os

res1 = base64.b64decode("bWFnZWR1LmNvbQ==".encode()).decode()
print(res1)

# 编码

res2 = base64.b64encode("[magedu.com](http://magedu.com)".encode()).decode()
print(res2)

"""
3. 列出本周讲的几种序列化方法，它们各自的特点是什么?
Pickle序列化：Pickle是Python中的一种特定格式的序列化，它可以将Python对象转换为字节流，以便存储在文件中或通过网络传输。Python内置的pickle模块提供了对Python对象的序列化和反序列化功能。
JSON（JavaScript Object Notation）序列化：JSON是一种轻量级的数据交换格式，它易于阅读和编写，并且广泛用于数据传输和存储。Python内置的json模块提供了将Python对象转换为JSON字符串（序列化）以及将JSON字符串转换回Python对象（反序列化）的功能。
MessagePack是一种二进制数据序列化格式，旨在提供高效的数据序列化和传输。它类似于JSON，但更紧凑、快速和高效。MessagePack的设计目标是通过减少数据的大小和序列化/反序列化的开销来提高性能。


4. 写一个Python脚本实现找到/tmp目录及其子目录下的以.htm结尾的文件，把其后缀名改为.html
"""

from pathlib import Path


def rename_htm_to_html(base_path):
    for i in os.listdir(base_path):
        d = base_path / i
        if d.is_dir():
            rename_htm_to_html(d)
        else:
            if d.suffix == ".htm":
                print(d)
                d.rename(d.with_suffix(".html"))


root = Path('/tmp')
rename_htm_to_html(root)
